﻿#pragma once

// UE Includes
#include "AIController.h"
#include "CoreMinimal.h"

#include "EQSWAIController.generated.h"

UCLASS()
class EQSWORKSHOP_API AEQSWAIController : public AAIController
{
	GENERATED_BODY()
};
