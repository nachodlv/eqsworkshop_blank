﻿#include "AIDataProvider_EQSCharacterTag.h"

void UAIDataProvider_EQSCharacterTag::BindData(const UObject& Owner, int32 RequestId)
{
	// We need to bing the UPROPERTY "Value" with its corresponding float from the ValuesByCharacterTag array

	// A good example con be seen in  UAIDataProvider_Random::BindData

	// 1. Iterate the ValuesByCharacterTag -> for (const FEQSValueByCharacterTag& ValueByCharacterTag : ValuesByCharacterTag)
	// 2. Grab the character tag by casting the Owner to AEQSWAICharacter
	// 3. Compare the character tag with the tag of the struct -> Character->GetCharacterTag().MatchesTag(ValueByCharacterTag.CharacterTag)
	// 4. Assign the UPROPERTY "Value" with its corresponding value -> Value = ValueByCharacterTag.Value

	// If we are testing with a TestingPawn then we'll not be able to cast to AEQSWAICharacter
	// We can just grab the first value from the array for now
}
