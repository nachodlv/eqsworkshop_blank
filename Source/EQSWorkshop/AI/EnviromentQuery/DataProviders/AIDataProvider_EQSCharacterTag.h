﻿#pragma once

#include "CoreMinimal.h"
#include "DataProviders/AIDataProvider.h"
#include "GameplayTagContainer.h"

#include "AIDataProvider_EQSCharacterTag.generated.h"

USTRUCT()
struct FEQSValueByCharacterTag
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, meta = (Categories = "Character.AI"))
	FGameplayTag CharacterTag;

	UPROPERTY(EditAnywhere)
	float Value;
};

/** Provides the Query with a float value depending on the character tag of its owner */
UCLASS()
class EQSWORKSHOP_API UAIDataProvider_EQSCharacterTag : public UAIDataProvider
{
	GENERATED_BODY()

public:
	virtual void BindData(const UObject& Owner, int32 RequestId) override;
	
private:
	/** The value that will be used by the EQS.
	* FAIDataProviderValue::GetMatchingProperties gets the properties by reflection that's why we need this UPROPERTY */
	UPROPERTY()
	float Value;

	/** The value for each character tag  */
	UPROPERTY(EditAnywhere, Category = "EQS")
	TArray<FEQSValueByCharacterTag> ValuesByCharacterTag;
};
