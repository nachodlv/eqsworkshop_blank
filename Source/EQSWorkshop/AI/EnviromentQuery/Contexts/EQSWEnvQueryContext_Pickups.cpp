﻿#include "EQSWEnvQueryContext_Pickups.h"

#include "EngineUtils.h"

#include "EnvironmentQuery/EnvQueryTypes.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "EQSWorkshop/Spawner/EQSPickup.h"

void UEQSWEnvQueryContext_Pickups::ProvideContext(FEnvQueryInstance& QueryInstance,
	FEnvQueryContextData& ContextData) const
{
	Super::ProvideContext(QueryInstance, ContextData);
	TArray<AActor*> Actors;
	for (auto It = TActorIterator<AEQSPickup>(QueryInstance.World); It; ++It)
	{
		Actors.Add(*It);
	}
	UEnvQueryItemType_Actor::SetContextHelper(ContextData, Actors);
}
