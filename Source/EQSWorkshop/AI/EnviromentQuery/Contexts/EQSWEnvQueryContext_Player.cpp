﻿#include "EQSWEnvQueryContext_Player.h"

// UE Includes
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Point.h"

void UEQSWEnvQueryContext_Player::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
	if (const APlayerController* PlayerController = QueryInstance.World->GetFirstPlayerController())
	{
		if (const APawn* Pawn = PlayerController->GetPawn())
		{
			UEnvQueryItemType_Actor::SetContextHelper(ContextData, Pawn);
		}
		else
		{
			UEnvQueryItemType_Point::SetContextHelper(ContextData, FVector());
		}
	}
	else
	{
		UEnvQueryItemType_Actor::SetContextHelper(ContextData, Cast<AActor>(QueryInstance.Owner.Get()));
	}
}


