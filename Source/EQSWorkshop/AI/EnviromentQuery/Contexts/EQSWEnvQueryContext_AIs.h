﻿#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryContext.h"

#include "EQSWEnvQueryContext_AIs.generated.h"

/** Provides the query with the AIs currently present in the world */
UCLASS()
class EQSWORKSHOP_API UEQSWEnvQueryContext_AIs : public UEnvQueryContext
{
	GENERATED_BODY()

public:
	virtual void ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const override;
};
