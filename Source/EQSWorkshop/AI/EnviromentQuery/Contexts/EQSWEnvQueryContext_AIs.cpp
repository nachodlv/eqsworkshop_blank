﻿#include "EQSWEnvQueryContext_AIs.h"

#include "EngineUtils.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "EQSWorkshop/AI/EQSWAICharacter.h"

void UEQSWEnvQueryContext_AIs::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
	// Iterates through all the AEQSWAICharacter actors of the world with the TActorItertor
	// Add them with the following function UEnvQueryItemType_Actor::SetContextHelper(ContextData, Actors);

	// A perfect example for this context is UEQSWEnvQueryContext_Pickups::ProvideContext
	// We just need to replace the Pickups with the AEQSWAICharacter
}
