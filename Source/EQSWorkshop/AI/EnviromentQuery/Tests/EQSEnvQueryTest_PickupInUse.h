﻿#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTest.h"

#include "EQSEnvQueryTest_PickupInUse.generated.h"

UCLASS()
class EQSWORKSHOP_API UEQSEnvQueryTest_PickupInUse : public UEnvQueryTest
{
	GENERATED_BODY()

public:
	UEQSEnvQueryTest_PickupInUse();

	// The method that iterates through the items and evaluates them
	virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;
};
