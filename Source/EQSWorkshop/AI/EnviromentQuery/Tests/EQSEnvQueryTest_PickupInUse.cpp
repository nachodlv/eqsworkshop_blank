﻿#include "EQSEnvQueryTest_PickupInUse.h"

#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"

UEQSEnvQueryTest_PickupInUse::UEQSEnvQueryTest_PickupInUse()
{
	ValidItemType = UEnvQueryItemType_Actor::StaticClass();

	// We set up the test cost so it can be sorted later
	Cost = EEnvTestCost::Low;

	// Whether we use float when evaluating an item (as pathfinding or distance) or we are using bools (as traces)
	SetWorkOnFloatValues(false);
}

void UEQSEnvQueryTest_PickupInUse::RunTest(FEnvQueryInstance& QueryInstance) const
{
	// This is how we retrieved the expected value our item is expected to have so it can pass the test
	BoolValue.BindData(QueryInstance.Owner.Get(), QueryInstance.QueryID);
	const bool bExpectedScore = BoolValue.GetValue();

	// To score the items you should follow the steps
	// 1. Iterate the items -> for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It)
	// 2. Retrieve the item as a Pickup -> QueryInstance.GetItemAsActor(It.GetIndex())
	// 3. Set the score of the item accordingly -> It.SetScore(TestPurpose, FilterType, Pickup->GetActorUsingPickup() != nullptr, bExpectedScore)

	// A good example is of how this can be accomplished it looking at UEnvQueryTest_GameplayTags::RunTest
}
