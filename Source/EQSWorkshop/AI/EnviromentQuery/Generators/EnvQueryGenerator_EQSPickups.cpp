﻿#include "EnvQueryGenerator_EQSPickups.h"
#include "EngineUtils.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Point.h"

UEnvQueryGenerator_EQSPickups::UEnvQueryGenerator_EQSPickups()
{
	ItemType = UEnvQueryItemType_Actor::StaticClass();
}

void UEnvQueryGenerator_EQSPickups::GenerateItems(FEnvQueryInstance& QueryInstance) const
{
	// To add items to the query instance use the following method:
	// FEnvQueryInstance::AddItemData
	
	// Example to add Actors
	// AActor* Actor;
	// QueryInstance.AddItemData<UEnvQueryItemType_Actor>(Actor)

	// Example to add Vector
	// FVector Location;
	// QueryInstance.AddItemData<UEnvQueryItemType_Point>(Location)

	// To retrieve all the pickups
	// 1. Get the game mode: QueryInstance.World->GetAuthGameMode<AEQSWorkshopGameMode>()
	// 2. Get the Pickup spawner AEQSWorkshopGameMode::GetPickupSpawner
	// 3. Get the pickups UEQSPickupSpawner::GetPickups
	// 4. Add each pickup to the query instance QueryInstance.AddItemData<UEnvQueryItemType_Actor>(Pickup)

}
