﻿#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryGenerator.h"

#include "EnvQueryGenerator_EQSPickups.generated.h"

/**
 * 
 */
UCLASS()
class EQSWORKSHOP_API UEnvQueryGenerator_EQSPickups : public UEnvQueryGenerator
{
	GENERATED_BODY()

public:
	UEnvQueryGenerator_EQSPickups();
	
	virtual void GenerateItems(FEnvQueryInstance& QueryInstance) const override;
};
