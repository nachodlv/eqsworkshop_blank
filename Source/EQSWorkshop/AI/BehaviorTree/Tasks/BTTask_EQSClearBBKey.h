﻿#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"

#include "BTTask_EQSClearBBKey.generated.h"

UCLASS()
class EQSWORKSHOP_API UBTTask_EQSClearBBKey : public UBTTaskNode
{
	GENERATED_BODY()

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
private:
	UPROPERTY(EditAnywhere, Category = "EQS")
	FBlackboardKeySelector BBToClear;
};
