﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EQSPickup.generated.h"

UCLASS()
class EQSWORKSHOP_API AEQSPickup : public AActor
{
	GENERATED_BODY()

public:
	AEQSPickup();

	UFUNCTION(BlueprintPure)
	AActor* GetActorUsingPickup() const { return InUsedBy; }

	UFUNCTION(BlueprintCallable)
	void SetActorUsingPickup(AActor* User) { InUsedBy = User; } 

	UFUNCTION(BlueprintPure)
	float GetUseTime() const { return UseTime; }
	
private:
	UPROPERTY(EditAnywhere, Category = "EQS")
	float UseTime = 10.0f;
	
	UPROPERTY(Transient)
	TObjectPtr<AActor> InUsedBy = nullptr;

};
